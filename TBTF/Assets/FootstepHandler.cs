﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepHandler : MonoBehaviour {

    float lastFramePosX;
    public float threshold;
    float lastFramePosZ;
    public AudioSource f_Audio;



    // Use this for initialization
    void Start () {
        lastFramePosX = this.transform.position.x;
        lastFramePosZ = this.transform.position.z;
        f_Audio = GetComponent<AudioSource>();
        f_Audio.Play();
    }
	
	// Update is called once per frame
	void Update () {
        if (Mathf.Abs(lastFramePosX - this.transform.position.x) > threshold || Mathf.Abs(lastFramePosZ - this.transform.position.z) > threshold)
        {
            f_Audio.UnPause();
        }
        else
        {
            f_Audio.Pause();
        }
        lastFramePosX = this.transform.position.x;
        lastFramePosZ = this.transform.position.z;
    }
}
