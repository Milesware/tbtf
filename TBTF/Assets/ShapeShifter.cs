﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeShifter : MonoBehaviour {

    float timer;
    public GameObject board;

	// Use this for initialization
	void Start () {
        timer = 0.0f;
	}

    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime;
        Debug.Log("timer: " + timer.ToString());
        if(timer >= 10 && timer <= 13)
        {
        // Debug.Log("Inside!");
            board.transform.localScale -= new Vector3(0.1f, 0, 0);
        }

        if (timer >= 30 && timer <= 33)
        {
            // Debug.Log("Inside!");
            board.transform.localScale += new Vector3(0, 0, 0.2f);
        }

        if (timer >= 50 && timer <= 53)
        {
            // Debug.Log("Inside!");
            board.transform.localScale += new Vector3(0.3f, 0, 0);
            board.transform.localScale -= new Vector3(0, 0, 0.3f);
        }

        if (timer >= 70 && timer <= 73)
        {
            // Debug.Log("Inside!");
            board.transform.localScale -= new Vector3(0.5f, 0, 0.2f);
        }
    }
}
