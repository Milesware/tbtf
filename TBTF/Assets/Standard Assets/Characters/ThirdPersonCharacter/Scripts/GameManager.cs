﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Dictionary<string, bool> m_playerStates = new Dictionary<string, bool>()
    {
        { "Red", true },
        { "Blue", true },
        { "Green", true }
    };
    public Text m_endText;
    private int m_playerCount;
	
    // Use this for initialization
	void Start ()
    {
        m_playerCount = m_playerStates.Count;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(m_playerCount == 1 && !m_endText.enabled)
        {
            foreach(KeyValuePair<string, bool> pair in m_playerStates)
            {
                if(pair.Value)
                {
                    m_endText.text = pair.Key + " Win!";
                    m_endText.enabled = true;
                    break;
                }
            }
        }
	}

    public void UpdatePlayerState(string name, bool state)
    {
        m_playerStates[name] = state;
        if (state) m_playerCount++;
        else m_playerCount--;
    }
}
