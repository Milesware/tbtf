﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour {

    public GameObject[] AllEthan;
    public GameObject HealthBox;
    public float fullHealth = 500;
    public float health;
    public GameObject HealthBar;
    float lastFramePosX;
    float lastFramePosY;
    // Use this for initialization
    void Start () {
        health = 500;
        lastFramePosX = this.transform.position.x;
        lastFramePosY = this.transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        decrementHealth();
        if (health <= 0)
        {
            foreach(GameObject i in AllEthan)
            {
                i.GetComponent<SkinnedMeshRenderer>().enabled = false;
                Destroy(i);
            }
            
        }
        if (Mathf.Abs(lastFramePosX - this.transform.position.x) > 0.05)
        {
            if(health <= 500)
            {
                health += 2;
            }
        }
        else if(Mathf.Abs(lastFramePosY - this.transform.position.y) > 0.05)
        {
            if(health <= 500)
            {
                health += 2;
            }
        }
        lastFramePosX = this.transform.position.x;
        lastFramePosY = this.transform.position.y;
    }

    public void decrementHealth()
    {
        if(health > 0)
        {
            health -= 1;
            HealthBar.transform.localScale = new Vector3(health / fullHealth, 1, 1);
        }
    }

    public float getHealth()
    {
        return health;
    }

    

}
