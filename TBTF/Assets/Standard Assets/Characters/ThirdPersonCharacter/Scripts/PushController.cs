﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PushController : MonoBehaviour {

    private UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter m_Character;
    private Rigidbody m_rigid;
    bool m_isBox;

	// Use this for initialization
	void Start () 
    {
        m_rigid = GetComponent<Rigidbody>();
        m_Character = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();
        if (m_Character == null)
        {
            m_isBox = true;
        }
        else
        {
            m_isBox = false;
        }
	}

    public void DoPush(Vector3 fCenter, float fInAir, float fGrd, float fBox, float dis)
    {
        if (m_isBox)
        {
            m_rigid.AddExplosionForce(fBox, fCenter, dis);
        }
        else
        {
            if (m_Character.isGrounded())
            {
                m_rigid.AddExplosionForce(fGrd, fCenter, dis);
            }
            else
            {
                m_rigid.AddExplosionForce(fInAir, fCenter, dis);
            }
        }
    }
       
   
}
