﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour {

    public Vector3 size;
    public Vector3 center;
    public float m_Height;
    public float stdMass;

    public GameObject[] toSpawns;
    public int[] numSpawns;
    public bool[] isRandomizd;
    public float[] spawnTimes;
    public float[] liveTimes;

    private int index;
    private float timer;

	// Use this for initialization
	void Start () {
        timer = 0.0f;
        index = 0;
    }
	
	// Update is called once per frame
	void Update () 
    {
        timer += Time.deltaTime;
        if(index < toSpawns.Length && timer >= spawnTimes[index])
        {
            Spawn();
            index++;
        }
	}

    void Spawn()
    {
        for (int i = 0; i < numSpawns[index]; ++i)
        {

            Vector3 pos = toSpawns[index].GetComponent<Transform>().position;
            Vector3 scale = toSpawns[index].GetComponent<Transform>().localScale;
            if (isRandomizd[index])
            {
                pos = new Vector3(Random.Range(-size.x / 2, size.x / 2), m_Height, Random.Range(-size.x / 2, size.x / 2));
                scale = new Vector3(Random.Range(0.5f, 5f), Random.Range(0.5f, 5f), Random.Range(0.5f, 5f));
            }
            float mass = stdMass * scale.x * scale.y * scale.z;

            GameObject newObject = Instantiate(toSpawns[index], pos, Quaternion.identity);
            newObject.GetComponent<Rigidbody>().mass = mass;
            DestroyObject(newObject, liveTimes[index]);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 0, 1, 0.5f);
        Gizmos.DrawCube(center, size);
    }
}
