using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        private float m_PushCounter = 0.0f;

        private AudioSource m_Audio;
        private GameManager m_gManager;


        public string m_PlayerName;

        public AudioClip m_PushSound;
        
        public float m_WalkVolume;
        public float m_PushVolume;

		public string HInput;
		public string VInput;
        public string FInput;
        public float m_PushDis;
        public float m_PushFactorPlayerInAir;
        public float m_PushFactorPlayerOnGrd;
        public float m_PushFactorBox;
        public float m_PushCoolDown;



        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
            m_Audio = GetComponent<AudioSource>();
            m_gManager = GameObject.FindObjectOfType<GameManager>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Trigger");
            if (other.gameObject.tag == "Boundary")
            {
                m_gManager.UpdatePlayerState(m_PlayerName, false);
            }
        }

        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis(HInput);
            float v = CrossPlatformInputManager.GetAxis(VInput);
            bool crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
                
            }
            
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            m_PushCounter += Time.deltaTime;
            if (CrossPlatformInputManager.GetButton(FInput) && m_PushCounter >= m_PushCoolDown)
            {
                Debug.Log("Fire");
                m_Audio.PlayOneShot(m_PushSound, m_PushVolume);
                GameObject[] objs = FindObjectsOfType<GameObject>();
                foreach (GameObject obj in objs)
                {
                    if (obj.GetInstanceID() != gameObject.GetInstanceID())
                    {
                        PushController objPush = obj.GetComponent<PushController>();
                        if (objPush == null)
                        {
                            continue;
                        }
                        else
                        {
                            objPush.DoPush(gameObject.transform.position, m_PushFactorPlayerInAir, 
                                            m_PushFactorPlayerOnGrd, m_PushFactorBox, m_PushDis);
                        }
                    }
                }
                m_PushCounter = 0.0f;
            }

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }
    }
}
