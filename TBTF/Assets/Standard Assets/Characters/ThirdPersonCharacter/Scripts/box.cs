﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class box : MonoBehaviour {

	public float life = 2f;
	public float lifeDropSpeed;
	public float lifeAddSpeed;
	public Transform player1;
	public Transform player2;

	public RectTransform bar1;
	public RectTransform bar2;
	public Text EndText;

	public GameObject owner;

	private float life1;
	private float life2;

	private bool Isplayer1Charging;
	private bool Isplayer2Charging;
	// Use this for initialization
	void Start () {
		life1 = life;
		life2 = life;
		owner = null;
	}
	
	// Update is called once per frame
	void Update () {
		life1 -= lifeDropSpeed;
		life2 -= lifeDropSpeed;

		if (transform.parent == player1) {
			life1 += lifeAddSpeed;
		}
		if (transform.parent == player2) {
			life2 += lifeAddSpeed;

		}

		if (life1 > life) {
			life1 = life;
		}
		if (life2 > life) {
			life2 = life;
		}

		if(life1<=0f){
			GamerOver(2);
		}
		if(life2<=0f){
			GamerOver(1);
		}

		bar1.localScale = new Vector3 (life1/life,1f,1f);
		bar2.localScale = new Vector3 (life2/life,1f,1f);

	}



	void GamerOver(int Winner){
		EndText.gameObject.SetActive (true);
		EndText.text = "Player" + Winner + "Win!";
	}




}
