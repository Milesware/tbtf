﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
	public float speed = 40.0F;
	//public float jumpSpeed = 8.0F;
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;
	private Rigidbody rb;

	public string HInput ="Horizontal" ;
	public string VInput = "Vertical";

	public string Operate = "Fire1";

	private bool boxed;
	private bool isCarrying;
	private GameObject box;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {


		//CharacterController controller = GetComponent<CharacterController>();
		//if (controller.isGrounded) {
		moveDirection = new Vector3(Input.GetAxis(HInput), 0, Input.GetAxis(VInput));
			//moveDirection = transform.TransformDirection(moveDirection);
			//moveDirection *= speed;
			//if (Input.GetButton("Jump"))
				//moveDirection.y = jumpSpeed;
		//}

		//moveDirection.y -= gravity * Time.deltaTime;
		moveDirection *= speed;
		print (boxed);

		rb.AddForce (moveDirection);

		if (Input.GetButtonDown (Operate)) {
			if (boxed) {
				box.transform.SetParent (transform);

				box.GetComponent<Rigidbody> ().isKinematic = true;
				box.GetComponent<BoxCollider> ().enabled = false;
				isCarrying = true;
			}
		}

		if (Input.GetButtonUp (Operate)) {
			if (isCarrying) {
				box.transform.SetParent (null);
				box.GetComponent<Rigidbody> ().isKinematic = false;
				box.GetComponent<BoxCollider> ().enabled = true;
				isCarrying = false;
				Debug.Log(box.transform);
			}
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.name == "Box") {
			boxed = true;
			box = other.gameObject;
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.name == "Box") {
			boxed = false;
			//box = other.gameObject;
		}
	}




}
