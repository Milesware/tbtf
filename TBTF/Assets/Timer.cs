﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text text;
    float time;

	// Use this for initialization
	void Start () {
        time = 0.0f;
        text.text = "Time: " + time.ToString() + "s";
        
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        text.text = "Time: " + time.ToString() + "s";
	}
}
